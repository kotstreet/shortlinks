﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using ShortLinks.WebAPI.Controllers;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.UnitTests.ControllerTest
{
    [TestFixture]
    public class StatisticsControllerTest
    {
        [Test]
        public async Task Show_WhenInvoked_ThenTopProjectsReturned()
        {
            // Arrange 
            var projects = new List<Project>()
            {
                new Project { Description = "sd 3", Caption = "capt e", OwnerId = 2, Id = 3 },
                new Project { Description = "sd 1", Caption = "capt 1", OwnerId = 3, Id = 1 },
                new Project { Description = "sd 2", Caption = "capt 2", OwnerId = 2, Id = 2 },
            };
            var statisticsService = new Mock<IStatisticsService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            statisticsService
                .Setup(service => service.GetTopProjectsAsync(It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(projects);

            var statisticsController = new StatisticsController(statisticsService.Object, personalDataProtectionService.Object);

            // Act
            var result = await statisticsController.Show();

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(projects));
        }

        [Test]
        public async Task AllLinks_WhenInvoked_ThenLinkStatisticModelReturned()
        {
            // Arrange 
            var linkStatisticModel = new LinkStatisticsModel();
            var statisticsService = new Mock<IStatisticsService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            statisticsService
                .Setup(service => service.GetOrderedUserLinksAsync(It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(linkStatisticModel);

            var statisticsController = new StatisticsController(statisticsService.Object, personalDataProtectionService.Object);

            // Act
            var result = await statisticsController.AllLinks();

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(linkStatisticModel));
        }

        [Test]
        public async Task LinksByProject_WhenProjectNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var statisticsService = new Mock<IStatisticsService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var statisticsController = new StatisticsController(statisticsService.Object, personalDataProtectionService.Object);

            // Act
            var result = await statisticsController.LinksByProject(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task LinksByProject_WhenUserHasNotTheProject_ThenRequestForbid()
        {
            // Arrange 
            var statisticsService = new Mock<IStatisticsService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var statisticsController = new StatisticsController(statisticsService.Object, personalDataProtectionService.Object);

            // Act
            var result = await statisticsController.LinksByProject(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task LinksByProject_WhenAllIsOK_ThenLinkStatisticsModelReturned()
        {
            // Arrange 
            var linkStatisticModel = new LinkStatisticsModel();
            var statisticsService = new Mock<IStatisticsService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            statisticsService
                .Setup(service => service.GetOrderedLinksByProjectAsync(It.IsAny<int>()))
                .ReturnsAsync(linkStatisticModel);

            var statisticsController = new StatisticsController(statisticsService.Object, personalDataProtectionService.Object);

            // Act
            var result = await statisticsController.LinksByProject(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(linkStatisticModel));
        }

        [Test]
        public async Task AllProjects_WhenInvoked_ThenProjectStatisticsModelReturned()
        {
            // Arrange 
            var projectStatisticModel = new ProjectStatisticsModel();
            var statisticsService = new Mock<IStatisticsService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            statisticsService
                .Setup(service => service.GetOrderedProjects(It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(projectStatisticModel);

            var statisticsController = new StatisticsController(statisticsService.Object, personalDataProtectionService.Object);

            // Act
            var result = await statisticsController.AllProjects();

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(projectStatisticModel));
        }

        [Test]
        public async Task ShowDiagram_WhenInvoked_ThenAllUserLinksReturned()
        {
            // Arrange 
            var links = new List<Link>();
            var statisticsService = new Mock<IStatisticsService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            statisticsService
                .Setup(service => service.GetLinksAsync(It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(links);

            var statisticsController = new StatisticsController(statisticsService.Object, personalDataProtectionService.Object);

            // Act
            var result = await statisticsController.ShowDiagram();

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(links));
        }
    }
}
