﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.WebAPI.Controllers;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.UnitTests.ControllerTest
{
    [TestFixture]
    public class ShortLinkControllerTest
    {
        [Test]
        public async Task RedirectToOldLink_WhenLinkNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var linkService = new Mock<ILinkService>();
            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var shortlinkController = new ShortLinkController(linkService.Object);

            // Act
            var result = await shortlinkController.RedirectToOldLink(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundObjectResult));
        }

        [Test]
        public async Task RedirectToOldLink_WhenLinkIsNotActive_ThenResourceNotFound()
        {
            // Arrange 
            var link = new Link { IsActive = false };
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            linkService
                .Setup(service => service.GetLinkAsync(It.IsAny<int>()))
                .ReturnsAsync(link);

            var shortlinkController = new ShortLinkController(linkService.Object);

            // Act
            var result = await shortlinkController.RedirectToOldLink(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundObjectResult));
        }

        [Test]
        public async Task RedirectToOldLink_WhenLinkIsActive_ThenOldLinkReturned()
        {
            // Arrange 
            var link = new Link { IsActive = true, OldLink = "old link" };
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            linkService
                .Setup(service => service.GetLinkAsync(It.IsAny<int>()))
                .ReturnsAsync(link);

            var shortlinkController = new ShortLinkController(linkService.Object);

            // Act
            var result = await shortlinkController.RedirectToOldLink(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(link.OldLink));
        }
    }
}
