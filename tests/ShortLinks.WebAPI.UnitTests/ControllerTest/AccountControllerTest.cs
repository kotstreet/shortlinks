﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ShortLinks.BLL.EF;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using ShortLinks.WebAPI.Controllers;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.UnitTests.ControllerTest
{
    [TestFixture]
    public class AccountControllerTest
    {
        [Test]
        public async Task Login_WhenModelIsNull_ReturnedBadRequest()
        {
            // Arrange 
            LoginModel model = null;
            IAccountService accountService = null;

            var accountController = new AccountController(accountService);

            // Act
            var result = await accountController.Login(model);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task Login_WhenModelStateIsNotValid_ReturnedBadRequest()
        {
            // Arrange 
            var model = new LoginModel();
            IAccountService accountService = null;

            var accountController = new AccountController(accountService);
            accountController.ModelState.AddModelError("", "some model error");

            // Act
            var result = await accountController.Login(model);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task Login_WhenUserNotExist_ReturnedBadRequest()
        {
            // Arrange 
            var model = new LoginModel { Email = "email", Password = "password" };
            User user = null;

            var accountService = new Mock<IAccountService>();
            accountService
                .Setup(service => service.FindUserAsync(It.IsAny<LoginModel>()))
                .ReturnsAsync(user);

            var accountController = new AccountController(accountService.Object);

            // Act
            var result = await accountController.Login(model);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }


        [Test]
        public async Task Login_WhenAllIsOK_ReturnedJWT()
        {
            // Arrange 
            var model = new LoginModel { Email = "user@example.com", Password = "string" };
            var user = new User { Email = "user@example.com", Password = "string", LastName = "string", FirstName = "string", Id = 4 };

            // It is a part for token without time part and verify signature.
            var expectedJWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1lIjoic3RyaW5nIHN0cmluZyIsImlkIjoiNCIsIm";

            var accountService = new Mock<IAccountService>();
            accountService
                .Setup(service => service.FindUserAsync(It.IsAny<LoginModel>()))
                .ReturnsAsync(user);

            var accountController = new AccountController(accountService.Object);

            // Act
            var result = await accountController.Login(model) as string;

            // Assert
            result.Should().Contain(expectedJWT);
        }

        [Test]
        public async Task Register_WhenModelIsNull_ReturnedBadRequest()
        {
            // Arrange 
            RegisterModel model = null;
            IAccountService accountService = null;

            var accountController = new AccountController(accountService);

            // Act
            var result = await accountController.Register(model);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task Register_WhenModelStateIsNotValid_ReturnedBadRequest()
        {
            // Arrange 
            var model = new RegisterModel();
            IAccountService accountService = null;

            var accountController = new AccountController(accountService);
            accountController.ModelState.AddModelError("", "some model error");

            // Act
            var result = await accountController.Register(model);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task Register_WhenEmailExist_ReturnedBadRequest()
        {
            // Arrange 
            var model = new RegisterModel();
            var accountService = new Mock<IAccountService>();

            accountService
                .Setup(service => service.IsEmailExistAsync(It.IsAny<string>()))
                .ReturnsAsync(true);
            var accountController = new AccountController(accountService.Object);

            // Act
            var result = await accountController.Register(model);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task Register_WhenAllIsOK_ReturnedJWT()
        {
            // Arrange 
            var model = new RegisterModel { Email = "user@example.com", Password = "string" };
            var user = new User { Email = "user@example.com", Password = "string", LastName = "string", FirstName = "string", Id = 4 };

            // It is a part for token without time part and verify signature.
            var expectedJWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1lIjoic3RyaW5nIHN0cmluZyIsImlkIjoiNCIsIm";
            
            var accountService = new Mock<IAccountService>();
            accountService
                .Setup(service => service.IsEmailExistAsync(It.IsAny<string>()))
                .ReturnsAsync(false);
            accountService
                .Setup(service => service.CreateUserAsync(It.IsAny<User>()));
            accountService
                .Setup(service => service.FindUserAsync(It.IsAny<string>()))
                .ReturnsAsync(user);

            var accountController = new AccountController(accountService.Object);

            // Act
            var result = await accountController.Register(model) as string;

            // Assert
            result.Should().Contain(expectedJWT);
        }
    }
}
