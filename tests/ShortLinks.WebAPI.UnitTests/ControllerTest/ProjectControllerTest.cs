﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.UnitTests.ControllerTest
{

    [TestFixture]
    public class ProjectControllerTest
    {
        [Test]
        public void Show_WhenInvoked_ThenProjectsReturned()
        {
            // Arrange 
            var projects = new List<Project>()
            {
                new Project { Description = "sd 1", Caption = "capt 1", OwnerId = 3, Id = 1 },
                new Project { Description = "sd 2", Caption = "capt 2", OwnerId = 2, Id = 2 },
                new Project { Description = "sd 3", Caption = "capt e", OwnerId = 2, Id = 3 },
            };
            var projectService = new Mock<IProjectService>();
            IPersonalDataProtectionService personalDataProtectionService = null;

            projectService
                .Setup(service => service.GetProjects(It.IsAny<ClaimsPrincipal>()))
                .Returns(projects);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService);

            // Act
            var result = projectController.Show();

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(projects));
        }

        [Test]
        public async Task Links_WhenProjectNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Links(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task Links_WhenUserHasNotTheProject_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Links(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task Links_WhenAllIsOK_ThenProjectReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            projectService
                .Setup(service => service.GetLinksByProjectIdAsync(It.IsAny<int>()))
                .ReturnsAsync(project);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Links(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(project));
        }

        [Test]
        public async Task Create_WhenModelIsNull_ThenBadRequestReturned()
        {
            // Arrange 
            Project project = null;
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Create(project);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task Create_WhenModelStateIsNotValid_ThenBadRequestReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);
            projectController.ModelState.AddModelError("", "some error");

            // Act
            var result = await projectController.Create(project);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task Create_WhenAllIsOk_ThenCreatedResultWithProjectReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Create(project);

            // Assert
            result.Should().BeEquivalentTo(new CreatedResult(nameof(ProjectController.Show), null));
        }

        [Test]
        public async Task EditGet_WhenProjectNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Edit(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task EditGet_WhenUserHasNotTheProject_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Edit(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task EditGet_WhenAllIsOK_ThenProjectReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            projectService
                .Setup(service => service.GetProjectAsync(It.IsAny<int>()))
                .ReturnsAsync(project);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Edit(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(project));
        }

        [Test]
        public async Task EditPut_WhenModelIsNull_ThenBadRequestReturned()
        {
            // Arrange 
            Project project = null;
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Edit(project);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task EditPut_WhenModelStateIsNotValid_ThenBadRequestReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);
            projectController.ModelState.AddModelError("", "some error");

            // Act
            var result = await projectController.Edit(project);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task EditPut_WhenProjectNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Edit(project);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task EditPut_WhenUserHasNotTheProject_ThenRequestForbid()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Edit(project);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task EditPut_WhenAllIsOK_ThenOkStatusReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Edit(project);

            // Assert
            result.Should().BeEquivalentTo(new OkResult());
        }

        [Test]
        public async Task ConfirmDelete_WhenProjectNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.ConfirmDelete(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task ConfirmDelete_WhenUserHasNotTheProject_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.ConfirmDelete(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task ConfirmDelete_WhenAllIsOK_ThenProjectReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            projectService
                .Setup(service => service.GetProjectAsync(It.IsAny<int>()))
                .ReturnsAsync(project);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.ConfirmDelete(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(project));
        }

        [Test]
        public async Task Delete_WhenProjectNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Delete(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task Delete_WhenUserHasNotTheProject_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Delete(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task Delete_WhenAllIsOK_ThenNoContentResultReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            projectService
                .Setup(service => service.GetProjectAsync(It.IsAny<int>()))
                .ReturnsAsync(project);

            var projectController = new ProjectController(projectService.Object, personalDataProtectionService.Object);

            // Act
            var result = await projectController.Delete(3);

            // Assert
            result.Should().BeEquivalentTo(new NoContentResult());
        }

    }
}
