﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using ShortLinks.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.UnitTests.ControllerTest
{
    [TestFixture]
    public class LinkControllerTest
    {
        [Test]
        public async Task AllLinks_WhenInvoked_ThenLinksReturned()
        {
            // Arrange 
            var now = DateTime.Now;
            var links = new List<Link>()
            {
                new Link { Description = "sd 1", OldLink = "old link 1", IsActive = false, IsPublic = true, DateOfCreate = now.AddMonths(-1), Id = 1 },
                new Link { Description = "sd 2", OldLink = "old link 2", IsActive = true, IsPublic = false, DateOfCreate = now.AddDays(-12), Id = 2 },
                new Link { Description = "sd 3", OldLink = "old link 3", IsActive = false, IsPublic = false, DateOfCreate = now.AddDays(-3), Id = 3 },
                new Link { Description = "sd 5", OldLink = "old link 5", IsActive = true, IsPublic = true, DateOfCreate = now.AddDays(-2), Id = 5 },
            };

            IProjectService projectService = null;
            IPersonalDataProtectionService personalDataProtectionService = null;
            var linkService = new Mock<ILinkService>();
            linkService
                .Setup(service => service.GetAllLinksAsync())
                .ReturnsAsync(links);

            var linkController = new LinkController(linkService.Object, projectService, personalDataProtectionService);

            // Act
            var result = await linkController.AllLinks();

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(links));
        }

        [Test]
        public async Task AddGet_WhenProjectNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Add(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task AddGet_WhenUserHasNotTheProject_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Add(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task AddGet_WhenAllIsOK_ThenProjectReturned()
        {
            // Arrange 
            var project = new Project();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            projectService
                .Setup(service => service.GetProjectAsync(It.IsAny<int>()))
                .ReturnsAsync(project);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Add(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(project));
        }

        [Test]
        public async Task AddPost_WhenModelIsNull_ThenBadRequestReturned()
        {
            // Arrange 
            LinkModel linkModel = null;
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Add(linkModel);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task AddPost_WhenModelStateIsNotValid_ThenBadRequestReturned()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);
            linkController.ModelState.AddModelError("", "some error");

            // Act
            var result = await linkController.Add(linkModel);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task AddPost_WhenProjectIsNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Add(linkModel);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task AddPost_WhenUserHasNotProject_ThenRequestForbid()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Add(linkModel);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task AddPost_WhenAllIsOK_ThenProjectIdReturned()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectId = 2;
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.AddLinkAsync(It.IsAny<LinkModel>()))
                .ReturnsAsync(projectId);


            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Add(linkModel);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(projectId));
        }

        [Test]
        public async Task ConfirmDeleteLink_WhenLinkNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.ConfirmDeleteLink(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task ConfirmDeleteLink_WhenUserHasNotLink_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.ConfirmDeleteLink(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task ConfirmDeleteLink_WhenAllIsOK_ThenLinkReturned()
        {
            // Arrange 
            var link = new Link();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.GetLinkAsync(It.IsAny<int>()))
                .ReturnsAsync(link);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.ConfirmDeleteLink(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(link));
        }

        [Test]
        public async Task Delete_WhenLinkNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Delete(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task Delete_WhenUserHasNotLink_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Delete(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task Delete_WhenAllIsOK_ThenProjectIdReturned()
        {
            // Arrange 
            var link = new Link { ProjectId = 10 };
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.GetLinkAsync(It.IsAny<int>()))
                .ReturnsAsync(link);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Delete(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(link.ProjectId));
        }

        [Test]
        public async Task EditGet_WhenLinkNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task EditGet_WhenUserHasNotLink_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task EditGet_WhenAllIsOK_ThenLinkReturned()
        {
            // Arrange 
            var link = new Link();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.GetLinkAsync(It.IsAny<int>()))
                .ReturnsAsync(link);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(link));
        }

        [Test]
        public async Task EditPost_WhenModelIsNull_ThenBadRequestReturned()
        {
            // Arrange 
            LinkModel linkModel = null;
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(linkModel);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task EditPost_WhenModelStateIsNotValid_ThenBadRequestReturned()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);
            linkController.ModelState.AddModelError("", "some error");

            // Act
            var result = await linkController.Edit(linkModel);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task EditPost_WhenProjectIsNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(linkModel);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task EditPost_WhenUserHasNotProject_ThenRequestForbid()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(linkModel);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }
        [Test]
        public async Task EditPost_WhenLinkIsNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(linkModel);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task EditPost_WhenUserHasNotLink_ThenRequestForbid()
        {
            // Arrange 
            var linkModel = new LinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(linkModel);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task EditPost_WhenAllIsOK_ThenProjectIdReturned()
        {
            // Arrange 
            var linkModel = new LinkModel { ProjectId = 2 };
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            personalDataProtectionService
                .Setup(service => service.IsProjectExists(It.IsAny<int>()))
                .Returns(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasProject(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Edit(linkModel);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(linkModel.ProjectId));
        }

        [Test]
        public async Task MoveGet_WhenLinkNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Move(3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task MoveGet_WhenUserHasNotLink_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Move(3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task MoveGet_WhenAllIsOK_ThenMoveLinkModelReturned()
        {
            // Arrange 
            var moveLinkModel = new MoveLinkModel();
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.CreateMoveLinkModelAsync(It.IsAny<int>()))
                .ReturnsAsync(moveLinkModel);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Move(3);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(moveLinkModel));
        }

        [Test]
        public async Task MovePost_WhenLinkNotExist_ThenResourceNotFound()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Move(3, 3);

            // Assert
            result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task MovePost_WhenUserHasNotLink_ThenRequestForbid()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Move(3, 3);

            // Assert
            result.Should().BeOfType(typeof(ForbidResult));
        }

        [Test]
        public async Task MovePost_WhenProjectHasNotLink_ThenBadRequestReturned()
        {
            // Arrange 
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.IsExistLinkInProjectAsync(It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(false);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Move(3, 3);

            // Assert
            result.Should().BeOfType(typeof(BadRequestObjectResult));
        }

        [Test]
        public async Task MovePost_WhenAllIsOK_ThenProjectIdReturned()
        {
            // Arrange 
            var moveLinkModel = new MoveLinkModel();
            var projectId = 2;
            var projectService = new Mock<IProjectService>();
            var personalDataProtectionService = new Mock<IPersonalDataProtectionService>();
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.IsLinkExistAsync(It.IsAny<int>()))
                .ReturnsAsync(true);
            personalDataProtectionService
                .Setup(service => service.IsUserHasLink(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            linkService
                .Setup(service => service.IsExistLinkInProjectAsync(It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(true);
            linkService
                .Setup(service => service.CreateMoveLinkModelAsync(It.IsAny<int>()))
                .ReturnsAsync(moveLinkModel);

            var linkController = new LinkController(linkService.Object,
                projectService.Object,
                personalDataProtectionService.Object);

            // Act
            var result = await linkController.Move(3, projectId);

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(projectId));
        }

        [Test]
        public async Task ShowMine_WhenInvoked_ThenLinksReturned()
        {
            // Arrange 
            var now = DateTime.Now;
            var links = new List<Link>()
            {
                new Link { Description = "sd 1", OldLink = "old link 1", IsActive = false, IsPublic = true, DateOfCreate = now.AddMonths(-1), Id = 1 },
                new Link { Description = "sd 5", OldLink = "old link 5", IsActive = true, IsPublic = true, DateOfCreate = now.AddDays(-2), Id = 5 },
            };

            IProjectService projectService = null;
            IPersonalDataProtectionService personalDataProtectionService = null;
            var linkService = new Mock<ILinkService>();

            linkService
                .Setup(service => service.FindMyLinksAsync(It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(links);

            var linkController = new LinkController(linkService.Object, projectService, personalDataProtectionService);

            // Act
            var result = await linkController.ShowMine();

            // Assert
            result.Should().BeEquivalentTo(new OkObjectResult(links));
        }
    }
}
