﻿using System;

namespace ShortLinks.BLL.Models
{
    /// <summary>
    /// Information aboute link.
    /// </summary>
    public class Link
    {
        /// <summary>
        /// Unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Full link(original link without any changes).
        /// </summary>
        public string OldLink { get; set; }

        /// <summary>
        /// Description of the link.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// State that decline is the link active now.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// State that decline is the link public.
        /// </summary>
        public bool IsPublic { get; set; }

        /// <summary>
        /// Date of added link in database.
        /// </summary>
        public DateTime DateOfCreate { get; set; }

        /// <summary>
        /// Count of transitions.
        /// </summary>
        public int UseCount { get; set; }

        /// <summary>
        /// Project witch consider the link.
        /// </summary>
        public Project Project { get; set; }

        /// <summary>
        /// Id the project.
        /// </summary>
        public int ProjectId { get; set; }
    }
}
