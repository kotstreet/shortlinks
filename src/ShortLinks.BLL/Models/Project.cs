﻿using ShortLinks.BLL.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShortLinks.BLL.Models
{
    /// <summary>
    /// It is a folder for some links with something common.
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Caption of rhe project.
        /// </summary>
        [Required(ErrorMessage = ModelAttribute.ProjectCaptionRequiredMessage)]
        public string Caption { get; set; }

        /// <summary>
        /// Descripton of the project.
        /// </summary>
        [Required(ErrorMessage = ModelAttribute.DescriptionRequiredMessage)]
        public string Description { get; set; }

        /// <summary>
        /// A user who has the project.
        /// </summary>
        public User Owner { get; set; }

        /// <summary>
        /// A user id.
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Links that in the project.
        /// </summary>
        public IEnumerable<Link> Links { get; set; }
    }
}
