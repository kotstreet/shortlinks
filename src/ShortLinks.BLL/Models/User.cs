﻿using System.Collections.Generic;

namespace ShortLinks.BLL.Models
{
    /// <summary>
    /// User in the service.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Email of the user.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// First name of the user.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the user.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Password of the user.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Projects that the user has.
        /// </summary>
        public IEnumerable<Project> Projects { get; set; }
    }
}
