﻿using ShortLinks.BLL.Constants;
using System.ComponentModel.DataAnnotations;

namespace ShortLinks.BLL.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = ModelAttribute.EmailRequiredMessage)]
        [EmailAddress(ErrorMessage = ModelAttribute.UnexistsEmailMessage)]
        public string Email { get; set; }

        [Required(ErrorMessage = ModelAttribute.PasswordRequiredMessage)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
