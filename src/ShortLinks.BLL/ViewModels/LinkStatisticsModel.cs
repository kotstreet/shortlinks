﻿using ShortLinks.BLL.Models;
using System;
using System.Collections.Generic;

namespace ShortLinks.BLL.ViewModels
{
    public class LinkStatisticsModel
    {
        public IEnumerable<Link> Links { get; set; }

        public int LinkCount { get; set; }

        public int TotalUseAmount { get; set; }

        public int ActiveLinkCount { get; set; }

        public int PublicLinkCount { get; set; }

        public DateTime FirstLinkDate { get; set; }

        public DateTime LastLinkDate { get; set; }
    }
}
