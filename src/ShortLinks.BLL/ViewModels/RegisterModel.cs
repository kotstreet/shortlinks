﻿using ShortLinks.BLL.Constants;
using System.ComponentModel.DataAnnotations;

namespace ShortLinks.BLL.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = ModelAttribute.EmailRequiredMessage)]
        [EmailAddress(ErrorMessage = ModelAttribute.UnexistsEmailMessage)]
        public string Email { get; set; }

        [Required(ErrorMessage = ModelAttribute.FirstNameRequiredMessage)]
        [RegularExpression(ModelAttribute.NameRegExp, ErrorMessage = ModelAttribute.IncorrectFirstNameMessage)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = ModelAttribute.SurnameRequiredMessage)]
        [RegularExpression(ModelAttribute.NameRegExp, ErrorMessage = ModelAttribute.IncorrectSurnameMessage)]
        public string LastName { get; set; }

        [Required(ErrorMessage = ModelAttribute.PasswordRequiredMessage)]
        [StringLength(ModelAttribute.MaxPasswordLength, MinimumLength = ModelAttribute.MinPasswordLength, ErrorMessage = ModelAttribute.PasswordLengthMessage)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare(ModelAttribute.PasswordFieldName, ErrorMessage = ModelAttribute.ConfirmPasswordMessage)]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
