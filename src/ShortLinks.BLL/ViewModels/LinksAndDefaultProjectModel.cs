﻿using ShortLinks.BLL.Models;
using System.Collections.Generic;

namespace ShortLinks.BLL.ViewModels
{
    public class LinksAndDefaultProjectModel
    {
        public IEnumerable<Link> Links { get; set; }
        public Project Project { get; set; }
    }
}
