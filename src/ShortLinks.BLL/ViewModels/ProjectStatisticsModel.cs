﻿using System.Collections.Generic;

namespace ShortLinks.BLL.ViewModels
{
    public class ProjectStatisticsModel
    {
        public List<ProjectModelSource> ProjectModels { get; set; }

        public int AmountCountLink { get; set; }

        public int AmountCountPublicLink { get; set; }

        public int AmountCountActiveLink { get; set; }

        public int AmountUseCount { get; set; }
    }
}
