﻿namespace ShortLinks.BLL.ViewModels
{
    public class ProjectModelSource
    {
        public string Caption { get; set; }
        public int CountLink { get; set; }
        public int CountPublicLink { get; set; }
        public int CountActiveLink { get; set; }
        public int UseCount { get; set; }
    }
}
