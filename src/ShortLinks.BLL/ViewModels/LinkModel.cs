﻿using ShortLinks.BLL.Constants;
using System.ComponentModel.DataAnnotations;

namespace ShortLinks.BLL.ViewModels
{
    public class LinkModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = ModelAttribute.OldLinkRequiredMessage)]
        [Url(ErrorMessage = ModelAttribute.UrlFieldMessage)]
        public string OldLink { get; set; }

        [Required(ErrorMessage = ModelAttribute.DescriptionRequiredMessage)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public bool IsPublic { get; set; }

        public int ProjectId { get; set; }
    }
}
