﻿using ShortLinks.BLL.Models;
using System.Collections.Generic;

namespace ShortLinks.BLL.ViewModels
{
    public class MoveLinkModel
    {
        public Link LinkForMove { get; set; }

        public IEnumerable<Project> MineProjects { get; set; }
    }
}
