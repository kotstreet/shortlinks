﻿using Microsoft.EntityFrameworkCore;
using ShortLinks.BLL.EF;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly ApplicationContext _db;

        public StatisticsService(ApplicationContext db)
        {
            _db = db;
        }

        public LinkStatisticsModel CreateLinkStatisticsModel(List<Link> links)
        {
            //find total use amount
            var totalUseAmount = 0;
            links.ForEach(link => totalUseAmount += link.UseCount);

            //find links count
            var linksCount = links.Count();

            //find active links count
            var activeLinksCount = links.Where(link => link.IsActive).Count();

            //find public links count
            var publicLinksCount = links.Where(link => link.IsPublic).Count();

            //find date of first created link
            var firstLinkDate = links.Min(link => link.DateOfCreate);

            //find date of last created link
            var lastLinkDate = links.Max(link => link.DateOfCreate);

            //create model fo show in view
            var linkStatisticsModel = new LinkStatisticsModel()
            {
                Links = links,
                LinkCount = linksCount,
                TotalUseAmount = totalUseAmount,
                ActiveLinkCount = activeLinksCount,
                PublicLinkCount = publicLinksCount,
                FirstLinkDate = firstLinkDate,
                LastLinkDate = lastLinkDate
            };

            return linkStatisticsModel;
        }

        public async Task<LinkStatisticsModel> GetOrderedLinksByProjectAsync(int projectId)
        {
            //create a list of the links
            var links = await (from project in _db.Projects
                               join link in _db.Links.Include(link => link.Project) on project.Id equals link.ProjectId
                               where project.Id == projectId
                               orderby link.UseCount descending, link.DateOfCreate
                               select link).ToListAsync();

            return CreateLinkStatisticsModel(links);
        }

        public async Task<ProjectStatisticsModel> GetOrderedProjects(ClaimsPrincipal claimUser)
        {
            //find current user email
            string userEmail = claimUser.Claims.First(claim => claim.Type == ClaimTypes.Email).Value; ;

            //create a list of the links
            var list = await (from user in _db.Users
                              join project in _db.Projects on user.Id equals project.OwnerId
                              where user.Email == userEmail
                              select new ProjectModelSource()
                              {
                                  Caption = project.Caption,
                                  CountLink = project.Links.Count(),
                                  CountActiveLink = project.Links.Where(link => link.IsActive).Count(),
                                  CountPublicLink = project.Links.Where(link => link.IsPublic).Count(),
                                  UseCount = project.Links.Sum(link => link.UseCount)
                              })
                       .OrderByDescending(model => model.UseCount)
                       .ThenBy(model => model.CountLink)
                       .ThenBy(model => model.Caption)
                       .ToListAsync();

            //create an object for display info in view
            var statistics = new ProjectStatisticsModel()
            {
                ProjectModels = list,
                AmountCountLink = list.Sum(model => model.CountLink),
                AmountCountActiveLink = list.Sum(model => model.CountActiveLink),
                AmountCountPublicLink = list.Sum(model => model.CountPublicLink),
                AmountUseCount = list.Sum(model => model.UseCount)
            };

            return statistics;
        }

        public async Task<LinkStatisticsModel> GetOrderedUserLinksAsync(ClaimsPrincipal claimUser)
        {
            //find current user email
            string userEmail = claimUser.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;

            //create a list of the links
            var links = await (from user in _db.Users
                               join project in _db.Projects on user.Id equals project.OwnerId
                               join link in _db.Links.Include(link => link.Project) on project.Id equals link.ProjectId
                               where user.Email == userEmail
                               orderby link.UseCount descending, link.DateOfCreate
                               select link).ToListAsync();

            return CreateLinkStatisticsModel(links);
        }

        public async Task<IEnumerable<Project>> GetTopProjectsAsync(ClaimsPrincipal claimUser)
        {
            //find user id
            var userEmail = claimUser.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
            var userId = (await _db.Users.FirstOrDefaultAsync(user => user.Email == userEmail)).Id;

            //create list of projects
            var projects = await _db.Projects.Where(project => project.OwnerId == userId).ToListAsync();
            return projects;
        }

        public async Task<IEnumerable<Link>> GetLinksAsync(ClaimsPrincipal claimUser)
        {
            //find current user email
            string userEmail = claimUser.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;

            //create a list of the links
            var links = await (from user in _db.Users
                               join project in _db.Projects on user.Id equals project.OwnerId
                               join link in _db.Links.Include(link => link.Project) on project.Id equals link.ProjectId
                               where user.Email == userEmail
                               orderby link.UseCount descending, link.DateOfCreate
                               select link)
                               .ToListAsync();

            return links;
        }
    }
}
