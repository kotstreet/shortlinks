﻿using Microsoft.EntityFrameworkCore;
using ShortLinks.BLL.EF;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly ApplicationContext _db;

        public AccountService(ApplicationContext db)
        {
            _db = db;
        }

        public async Task<User> FindUserAsync(string email)
        {
            return await _db.Users.FirstAsync(u => u.Email == email);
        }

        public async Task<User> FindUserAsync(LoginModel model)
        {
            return await _db.Users.FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == model.Password);
        }

        public async Task<int> CreateUserAsync(User user)
        {
            await _db.Users.AddAsync(user);
            return await _db.SaveChangesAsync();
        }

        public async Task<bool> IsEmailExistAsync(string email)
        {
            return await _db.Users.AnyAsync(user => user.Email == email);
        }
    }
}
