﻿using ShortLinks.BLL.Models;
using ShortLinks.BLL.ViewModels;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Add user into database.
        /// </summary>
        /// <param name="model">Register model with user info.</param>
        public Task AddUserIntoDBAsync(RegisterModel model);

        /// <summary>
        /// Create default project for a new user.
        /// </summary>
        /// <param name="user">New user.</param>
        /// <returns>Created project.</returns>
        public Project CreateDefaultProject(User user);
    }
}
