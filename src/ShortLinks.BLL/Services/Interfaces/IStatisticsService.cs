﻿using ShortLinks.BLL.Models;
using ShortLinks.BLL.ViewModels;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services.Interfaces
{
    public interface IStatisticsService
    {
        /// <summary>
        /// Get ordered projects for the user.
        /// </summary>
        /// <param name="claimUser">The user claims.</param>
        /// <returns>Ordered projects.</returns>
        public Task<ProjectStatisticsModel> GetOrderedProjects(ClaimsPrincipal claimUser);

        /// <summary>
        /// Get ordered links fro the project.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>Ordered links.</returns>
        public Task<LinkStatisticsModel> GetOrderedLinksByProjectAsync(int projectId);

        /// <summary>
        /// Top projects fro the user.
        /// </summary>
        /// <param name="claimUser">The user claims.</param>
        /// <returns>Top projects.</returns>
        public Task<IEnumerable<Project>> GetTopProjectsAsync(ClaimsPrincipal claimUser);

        /// <summary>
        /// Get links for the user.
        /// </summary>
        /// <param name="claimUser">The user claims.</param>
        /// <returns>Links.</returns>
        public Task<IEnumerable<Link>> GetLinksAsync(ClaimsPrincipal claimUser);

        /// <summary>
        /// Get ordered links for the user
        /// </summary>
        /// <param name="claimUser">The user claims.</param>
        /// <returns>Orderer links.</returns>
        public Task<LinkStatisticsModel> GetOrderedUserLinksAsync(ClaimsPrincipal claimUser);

        /// <summary>
        /// Create statistic for links.
        /// </summary>
        /// <param name="links">Links.</param>
        /// <returns>LinkStatisticsModel.</returns>
        public LinkStatisticsModel CreateLinkStatisticsModel(List<Link> links);
    }
}
