﻿using ShortLinks.BLL.Models;
using ShortLinks.BLL.ViewModels;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services.Interfaces
{
    public interface ILinkService
    {
        /// <summary>
        /// Delete the link.
        /// </summary>
        /// <param name="link">Link for delete.</param>
        public Task DeleteLinkAsync(Link link);

        /// <summary>
        /// Change info aboute link and save it.
        /// </summary>
        /// <param name="link">Link for save.</param>
        public Task SaveLinkAsync(LinkModel link);

        /// <summary>
        /// Move the link to the project.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <param name="projectId">The project id</param>
        public Task MoveLinkAsync(int linkId, int projectId);

        /// <summary>
        /// Find links for the user.
        /// </summary>
        /// <param name="claimUser">The user claims</param>
        /// <returns>List of links.</returns>
        public Task<List<Link>> FindMyLinksAsync(ClaimsPrincipal claimUser);

        /// <summary>
        /// Add a new link.
        /// </summary>
        /// <param name="model">Model of link for add.</param>
        /// <returns>The link prooject id.</returns>
        public Task<int> AddLinkAsync(LinkModel model);

        /// <summary>
        /// Get links and default project for the user.
        /// </summary>
        /// <param name="links">The links.</param>
        /// <param name="claimUser">Claim for the user.</param>
        /// <returns>Links and default project for the user.</returns>
        public Task<LinksAndDefaultProjectModel> GetLinksAndDefaulProjectModelAsync(IEnumerable<Link> links, ClaimsPrincipal claimUser);

        /// <summary>
        /// Check if exists the link in the project.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <param name="projectId">The project id.</param>
        /// <returns>True if exists, otherwise false.</returns>
        public Task<bool> IsExistLinkInProjectAsync(int linkId, int projectId);

        /// <summary>
        /// Check if the link exists.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <returns>True if exists, otherwise false.</returns>
        public Task<bool> IsLinkExistAsync(int linkId);

        /// <summary>
        /// Create move link model for movement link to another project.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <returns>MoveLinkModel.</returns>
        public Task<MoveLinkModel> CreateMoveLinkModelAsync(int linkId);

        /// <summary>
        /// Increace the link use count.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        public Task IncreaseInQuantityAsync(int linkId);

        /// <summary>
        /// Get list of all links.
        /// </summary>
        /// <returns>List of links.</returns>
        public Task<List<Link>> GetAllLinksAsync();

        /// <summary>
        /// Get the lin by id.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <returns>The link.</returns>
        public Task<Link> GetLinkAsync(int linkId);
    }
}
