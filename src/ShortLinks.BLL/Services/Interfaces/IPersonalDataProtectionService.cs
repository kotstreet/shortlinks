﻿using System.Security.Claims;

namespace ShortLinks.BLL.Services.Interfaces
{
    public interface IPersonalDataProtectionService
    {
        /// <summary>
        /// Check if current user has the link by link id
        /// </summary>
        /// <param name="linkId">link id</param>
        /// <param name="User">current authorization user</param>
        /// <returns>true if the user has the link</returns>
        public bool IsUserHasLink(int linkId, ClaimsPrincipal User);

        /// <summary>
        /// Check if current user has the projet by project id
        /// </summary>
        /// <param name="projectId">project id</param>
        /// <param name="User">current authorization user</param>
        /// <returns>true if the user has the project</returns>
        public bool IsUserHasProject(int projectId, ClaimsPrincipal User);

        /// <summary>
        /// Check if the project exist in the database.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>true if exist, otherwise false.</returns>
        public bool IsProjectExists(int projectId);
    }
}
