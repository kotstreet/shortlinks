﻿using ShortLinks.BLL.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services.Interfaces
{
    public interface IProjectService
    {
        /// <summary>
        /// Add a new project for the user.
        /// </summary>
        /// <param name="project">The project for add.</param>
        /// <param name="user">User claims.</param>
        public Task AddProjectAsync(Project project, ClaimsPrincipal user);

        /// <summary>
        /// Delete the project with all links.
        /// </summary>
        /// <param name="id">Id of project for delete</param>
        public Task DeleteProjectAsync(int id);

        /// <summary>
        /// Get all user projects.
        /// </summary>
        /// <param name="user">The user claims</param>
        /// <returns>User projects.</returns>
        public IEnumerable<Project> GetProjects(ClaimsPrincipal user);

        /// <summary>
        /// Get the project and he project links .
        /// </summary>
        /// <param name="id">The project id.</param>
        /// <returns>Project with links.</returns>
        public Task<Project> GetLinksByProjectIdAsync(int id);

        /// <summary>
        /// Save changes in project.
        /// </summary>
        /// <param name="project">The project.</param>
        public Task SaveProjectAsync(Project project);

        /// <summary>
        /// Get the project by id.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project.</returns>
        public Task<Project> GetProjectAsync(int projectId);
    }
}
