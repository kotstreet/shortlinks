﻿using ShortLinks.BLL.Models;
using ShortLinks.BLL.ViewModels;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services.Interfaces
{
    public interface IAccountService
    {
        /// <summary>
        /// Find the user in database from login model with the email and password.
        /// </summary>
        /// <param name="model">The login model.</param>
        /// <returns>The user.</returns>
        public Task<User> FindUserAsync(LoginModel model);

        /// <summary>
        /// Find the user in database by the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>The user.</returns>
        public Task<User> FindUserAsync(string email);

        /// <summary>
        /// Create new user in database.
        /// </summary>
        /// <param name="user">User for create.</param>
        /// <returns>One if all OK, otherwise something was wrong.</returns>
        public Task<int> CreateUserAsync(User user);

        /// <summary>
        /// Check if user with email exist in the database already.
        /// </summary>
        /// <param name="email">Email for check.</param>
        /// <returns>True if exist, otherwise false.</returns>
        public Task<bool> IsEmailExistAsync(string email);
    }
}
