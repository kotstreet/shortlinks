﻿using Microsoft.EntityFrameworkCore;
using ShortLinks.BLL.EF;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly ApplicationContext _db;

        public ProjectService(ApplicationContext db)
        {
            _db = db;
        }

        public async Task AddProjectAsync(Project project, ClaimsPrincipal claimUser)
        {
            //get current user
            var email = claimUser.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
            var user = await _db.Users.FirstOrDefaultAsync(user => user.Email == email);

            //set user for project
            project.Owner = user;

            //save data
            await _db.Projects.AddAsync(project);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteProjectAsync(int id)
        {
            //find project in db
            var project = await _db.Projects.FirstAsync(project => project.Id == id);

            //delete and save
            _db.Projects.Remove(project);
            await _db.SaveChangesAsync();
        }

        public async Task<Project> GetLinksByProjectIdAsync(int id)
        {
            //find project and load links for it
            var project = await _db.Projects.FirstAsync(project => project.Id == id);
            _db.Entry(project).Collection(proj => proj.Links).Load();

            return project;
        }

        public IEnumerable<Project> GetProjects(ClaimsPrincipal claimUser)
        {
            //get projects where user is current user
            var email = claimUser.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
            return _db.Projects.Where(project => project.Owner.Email == email).Include(project => project.Links);
        }

        public async Task SaveProjectAsync(Project project)
        {
            //find the project in db
            var currentProject = await _db.Projects.FirstOrDefaultAsync(proj => proj.Id == project.Id);

            //edit data of project
            currentProject.Caption = project.Caption;
            currentProject.Description = project.Description;

            //save changes
            await _db.SaveChangesAsync();
        }

        public async Task<Project> GetProjectAsync(int projectId)
        {
            return await _db.Projects.FirstAsync(project => project.Id == projectId);
        }
    }
}
