﻿using ShortLinks.BLL.EF;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services
{
    public class UserService : IUserService
    {
        private const string DefaultProjectCaption = "Default";
        private const string DefaultProjectDescription = "Проект созданный по умолчанию(в него попадают все ссылки созданные не из конкретного проекта)";

        private readonly ApplicationContext _db;

        public UserService(ApplicationContext db)
        {
            _db = db;
        }

        public async Task AddUserIntoDBAsync(RegisterModel model)
        {
            //create user
            var user = new User()
            {
                Email = model.Email,
                Password = model.Password,
                FirstName = model.FirstName,
                LastName = model.LastName
            };

            //save data in database
            await _db.Users.AddAsync(user);
            await _db.SaveChangesAsync();

            //create project
            var project = CreateDefaultProject(user);

            //save data in database
            await _db.Projects.AddAsync(project);
            await _db.SaveChangesAsync();
        }

        public Project CreateDefaultProject(User user)
        {
            //create default project
            var project = new Project()
            {
                Caption = DefaultProjectCaption,
                Description = DefaultProjectDescription,
                Owner = user
            };

            return project;
        }
    }
}
