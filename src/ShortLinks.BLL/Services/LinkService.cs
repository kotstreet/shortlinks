﻿using Microsoft.EntityFrameworkCore;
using ShortLinks.BLL.EF;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLinks.BLL.Services
{
    public class LinkService : ILinkService
    {
        private readonly ApplicationContext _db;

        public LinkService(ApplicationContext db)
        {
            _db = db;
        }

        public async Task DeleteLinkAsync(Link link)
        {
            _db.Links.Remove(link);
            await _db.SaveChangesAsync();
        }

        public async Task<List<Link>> FindMyLinksAsync(ClaimsPrincipal claimUser)
        {
            //find current user email
            string userEmail = claimUser.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;

            //create a list of the links
            var links = await (from user in _db.Users
                               join project in _db.Projects on user.Id equals project.OwnerId
                               join link in _db.Links.Include(link => link.Project) on project.Id equals link.ProjectId
                               where user.Email == userEmail
                               select link)
                        .ToListAsync();

            return links;
        }


        public async Task MoveLinkAsync(int linkId, int projectId)
        {
            // get link and project
            var link = await _db.Links.FirstAsync(link => link.Id == linkId);
            var project = await _db.Projects.FirstAsync(project => project.Id == projectId);

            //save changes
            link.Project = project;
            await _db.SaveChangesAsync();
        }

        public async Task SaveLinkAsync(LinkModel link)
        {
            //find the link in db
            var currentLink = await _db.Links.FirstAsync(curLink => curLink.Id == link.Id);

            //edit data of link
            currentLink.OldLink = link.OldLink;
            currentLink.Description = link.Description;
            currentLink.IsActive = link.IsActive;
            currentLink.IsPublic = link.IsPublic;

            //save changes
            await _db.SaveChangesAsync();
        }


        public async Task<int> AddLinkAsync(LinkModel model)
        {
            //create link and save one
            var link = new Link()
            {
                OldLink = model.OldLink,
                Description = model.Description,
                IsActive = model.IsActive,
                IsPublic = model.IsPublic,
                DateOfCreate = DateTime.Now,
                UseCount = 0,
                Project = await _db.Projects.FirstAsync(project => project.Id == model.ProjectId)
            };
            await _db.Links.AddAsync(link);
            await _db.SaveChangesAsync();

            return link.ProjectId;
        }

        public async Task<LinksAndDefaultProjectModel> GetLinksAndDefaulProjectModelAsync(IEnumerable<Link> links, ClaimsPrincipal claimUser)
        {
            Project project;

            //get default project 
            if (claimUser.Identity.IsAuthenticated)
            {
                //get default project for current user
                var userEmail1 = claimUser.Claims;
                string userEmail = userEmail1.First(claim => claim.Type == ClaimTypes.Email).Value;
                var user = _db.Users.First(user => user.Email == userEmail);
                project = await _db.Projects.FirstAsync(project => project.OwnerId == user.Id);
            }
            else
            {
                //default project is absent if the user is not authenticate
                project = null;
            }

            //create linkAndDefaultprojectModel
            var linkAndDefaultProject = new LinksAndDefaultProjectModel()
            {
                Project = project,
                Links = links
            };

            return linkAndDefaultProject;
        }

        public async Task<bool> IsExistLinkInProjectAsync(int linkId, int projectId)
        {
            var link = await _db.Links.FindAsync(linkId);
            return link != null && link.ProjectId == projectId;
        }

        public async Task<bool> IsLinkExistAsync(int linkId)
        {
            var link = await _db.Links.FindAsync(linkId);
            return link != null;
        }

        public async Task<MoveLinkModel> CreateMoveLinkModelAsync(int linkId)
        {
            //get link and project
            var link = await _db.Links.FirstAsync(link => link.Id == linkId);
            var project = await _db.Projects.FirstAsync(project => project.Id == link.ProjectId);

            //create list of projects
            var list = await _db.Projects
                .Where(proj => proj.OwnerId == project.OwnerId)
                .ToListAsync();

            return new MoveLinkModel
            {
                LinkForMove = link,
                MineProjects = list,
            };
        }

        public async Task IncreaseInQuantityAsync(int linkId)
        {
            var link = await _db.Links.FirstOrDefaultAsync(link => link.Id == linkId);

            link.UseCount++;
            await _db.SaveChangesAsync();
        }

        public async Task<List<Link>> GetAllLinksAsync()
        {
            return await _db.Links.ToListAsync();
        }

        public async Task<Link> GetLinkAsync(int linkId)
        {
            return await _db.Links.FirstOrDefaultAsync(link => link.Id == linkId);
        }
    }
}
