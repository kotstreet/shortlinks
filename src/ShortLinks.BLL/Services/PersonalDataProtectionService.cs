﻿using ShortLinks.BLL.EF;
using ShortLinks.BLL.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace ShortLinks.BLL.Services
{
    public class PersonalDataProtectionService : IPersonalDataProtectionService
    {
        private readonly ApplicationContext _db;
        public PersonalDataProtectionService(ApplicationContext db)
        {
            _db = db;
        }

        public bool IsUserHasLink(int linkId, ClaimsPrincipal User)
        {
            //find user
            var userId = FindUserId(User);

            //check if the user has the link
            return _db.Links
                .Join(_db.Projects,
                    link => link.ProjectId,
                    project => project.Id,
                    (link, project) => new { LinkId = link.Id, UserId = project.OwnerId })
                .FirstOrDefault(obj => obj.LinkId == linkId)
                .UserId == userId;
        }

        public bool IsProjectExists(int projectId)
        {
            return _db.Projects
                .Any(project => project.Id == projectId);
        }

        public bool IsUserHasProject(int projectId, ClaimsPrincipal User)
        {
            //find user
            var userId = FindUserId(User);

            //check if the user has the project
            return _db.Projects
                .FirstOrDefault(project => project.Id == projectId)
                ?.OwnerId == userId;
        }

        private int FindUserId(ClaimsPrincipal User)
        {
            var mail = User.Claims
                .FirstOrDefault(claim => claim.Type == ClaimTypes.Email)
                .Value;
            return _db.Users
                .FirstOrDefault(user => user.Email == mail)
                .Id;
        }
    }
}