﻿namespace ShortLinks.BLL.Constants
{
    /// <summary>
    /// Constants for model validation attributes.
    /// </summary>
    public static class ModelAttribute
    {
        // Limits
        public const int MinPasswordLength = 6;

        public const int MaxPasswordLength = 20;

        // Fields
        public const string PasswordFieldName = "Password";

        // Regular expressions
        public const string NameRegExp = @"[A-Za-z]{2,}|[А-Яа-я]{2,}";

        // Messages
        public const string ProjectCaptionRequiredMessage = "Заполните название";

        public const string OldLinkRequiredMessage = "Укажите ссылку";

        public const string FirstNameRequiredMessage = "Не указано имя";

        public const string SurnameRequiredMessage = "Не указана фамилия";

        public const string EmailRequiredMessage = "Не указан Email";

        public const string PasswordRequiredMessage = "Не указан пароль";

        public const string PasswordLengthMessage = "Длинна парооля должна быть от 6 до 20 символов";

        public const string ConfirmPasswordMessage = "Пароли не совпадает";

        public const string UnexistsEmailMessage = "Введите существующий адрес электронной почты";

        public const string IncorrectFirstNameMessage = "Неправдоподобное имя(только английский или русский)";

        public const string IncorrectSurnameMessage = "Неправдоподобная фамилия(только английский или русский)";

        public const string DescriptionRequiredMessage = "Заполните описание";
        
        public const string UrlFieldMessage = "В данное поле должна быть занесена ссылка";

    }
}
