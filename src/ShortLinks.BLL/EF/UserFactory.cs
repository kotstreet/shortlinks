﻿using ShortLinks.BLL.Models;
using System.Collections.Generic;

namespace ShortLinks.BLL.EF
{
    /// <summary>
    /// It's a factory for initialize database with default users.
    /// </summary>
    public static class UserFactory
    {
        /// <summary>
        /// Get default users.
        /// </summary>
        /// <returns>Users for initialize.</returns>
        public static IEnumerable<User> GetDefaultUsers()
        {
            return new User[]
                {
                    new User() {Id = 1, Email = "antosha@mail.ru", FirstName = "Anton", LastName = "Loginov", Password = "123321" },
                    new User() {Id = 2, Email = "aniutic@mail.ru", FirstName = "Ann", LastName = "Spodobaeva", Password = "123123" },
                    new User() {Id = 3, Email = "andrei@mail.ru", FirstName = "Andrei", LastName = "Goncharov", Password = "123123" }
                };
        }
    }
}
