﻿using ShortLinks.BLL.Models;
using System.Collections.Generic;

namespace ShortLinks.BLL.EF
{
    /// <summary>
    /// It's a factory for initialize database with default projects.
    /// </summary>
    public static class ProjectFactory
    {
        /// <summary>
        /// Get default projects.
        /// </summary>
        /// <returns>Projects for initialize.</returns>
        public static IEnumerable<Project> GetDefaultsProjects()
        {
            return new Project[]
                {
                    new Project() {Id = 1, Caption = "Default", Description = "Проект по умолчанию", OwnerId = 1},
                    new Project() {Id = 2, Caption = "Default", Description = "Проект по умолчанию", OwnerId = 2},
                    new Project() {Id = 3, Caption = "Default", Description = "Проект по умолчанию", OwnerId = 3},
                    new Project() {Id = 4, Caption = "Погода", Description = " все данные о погоде", OwnerId = 1},
                    new Project() {Id = 5, Caption = "Новости", Description = "Новости мира", OwnerId = 1},
                    new Project() {Id = 6, Caption = "Lamoda", Description = "одежда женская", OwnerId = 2},
                    new Project() {Id = 7, Caption = "Семья", Description = "надежда на будущее", OwnerId = 3},
                };
        }
    }
}
