﻿using Microsoft.EntityFrameworkCore;
using ShortLinks.BLL.Models;

namespace ShortLinks.BLL.EF
{
    /// <summary>
    /// Database context for link amanagement.
    /// </summary>
    public class ApplicationContext : DbContext
    {
        /// <summary>
        /// The set of users.
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// The set of projects.
        /// </summary>
        public DbSet<Project> Projects { get; set; }

        /// <summary>
        /// The set of links.
        /// </summary>
        public DbSet<Link> Links { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<User>()
                .HasData(UserFactory.GetDefaultUsers());

            modelBuilder
                .Entity<Project>()
                .HasData(ProjectFactory.GetDefaultsProjects());

            modelBuilder
                .Entity<Link>()
                .HasData(LinkFactory.GetDefaultLinks());
        }
    }
}
