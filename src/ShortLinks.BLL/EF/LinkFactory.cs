﻿using ShortLinks.BLL.Models;
using System;
using System.Collections.Generic;

namespace ShortLinks.BLL.EF
{
    /// <summary>
    /// It's a factory for initialize database with default links.
    /// </summary>
    public static class LinkFactory
    {
        /// <summary>
        /// Get default links.
        /// </summary>
        /// <returns>Links for initialize.</returns>
        public static IEnumerable<Link> GetDefaultLinks()
        {
            return new Link[]
               {
                    new Link() {Id = 1, OldLink = "https://community-z.com/signin",
                        Description = "Зум, вход",
                        IsActive = true, IsPublic = false, DateOfCreate = DateTime.Now, UseCount = 20, ProjectId = 1},
                    new Link() {Id = 2, OldLink = "https://tproger.ru/curriculum/git-guide/",
                        Description = "Учить гит",
                        IsActive = true, IsPublic = true, DateOfCreate = DateTime.Now.AddDays(-3), UseCount = 12, ProjectId = 1},
                    new Link() {Id = 3, OldLink = "https://3dtoday.ru/blogs/saamec/the-program-blender-from-scratch-or-as-a-week-to-learn-how-to-model-un",
                         Description = "Блендер",
                        IsActive = false, IsPublic = false, DateOfCreate = DateTime.Now.AddDays(-12), UseCount = 20, ProjectId = 1},
                    new Link() {Id = 4, OldLink = "http://old.gsu.by/info/scripts/pageh.asp?id=korotkevitch",
                        Description = "Короткевич, БД",
                        IsActive = true, IsPublic = false, DateOfCreate = DateTime.Now.AddDays(-4), UseCount = 6, ProjectId = 1},
                    new Link() {Id = 5, OldLink = "https://habr.com/ru/company/ods/blog/322076/",
                        Description = "База машинного обучения",
                        IsActive = true, IsPublic = false, DateOfCreate = DateTime.Now.AddDays(-22), UseCount = 44, ProjectId = 1},
                    new Link() {Id = 6, OldLink = "https://aliexpress.ru/popular/%25D0%2597%25D0%25B3%25D1%258C-%25D0%25B0%25D0%25B2%25D1%2582%25D0%25BE%25D0%25BC%25D0%25B0%25D0%25B3%25D0%25BD%25D0%25B8%25D1%2582%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B9-remote-control.html?site=rus&CatId=0&brandValueIds=201563129&SearchText=Згь+автомагнитолой+remote+control&needQuery=n&g=y",
                        Description = "Алиекспресс пульт",
                        IsActive = true, IsPublic = true, DateOfCreate = DateTime.Now.AddDays(-3), UseCount = 43, ProjectId = 1},
                    new Link() {Id = 7, OldLink = "https://login.aliexpress.ru/?spm=a2g0v.search0302.1000002.4.2ade5fd4h9tsWF&return=https%3A%2F%2Faliexpress.ru%2Fpopular%2F%2525D0%252597%2525D0%2525B3%2525D1%25258C-%2525D0%2525B0%2525D0%2525B2%2525D1%252582%2525D0%2525BE%2525D0%2525BC%2525D0%2525B0%2525D0%2525B3%2525D0%2525BD%2525D0%2525B8%2525D1%252582%2525D0%2525BE%2525D0%2525BB%2525D0%2525BE%2525D0%2525B9-remote-control.html%3Fsite%3Drus%26CatId%3D0%26brandValueIds%3D201563129%26SearchText%3D%25D0%2597%25D0%25B3%25D1%258C%2B%25D0%25B0%25D0%25B2%25D1%2582%25D0%25BE%25D0%25BC%25D0%25B0%25D0%25B3%25D0%25BD%25D0%25B8%25D1%2582%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B9%2Bremote%2Bcontrol%26needQuery%3Dn%26g%3Dy&from=lighthouse&_ga=2.102647297.1468647020.1593290882-880985870.1592948887&random=A52EE51ADA36B7C012DAD4FE20C03128",
                        Description = "Алиекспресс, вход",
                        IsActive = false, IsPublic = true, DateOfCreate = DateTime.Now.AddDays(-2), UseCount = 1, ProjectId = 2},
                    new Link() {Id = 8, OldLink = "https://wooordhunt.ru/word/neglect",
                        Description = "Перевод neglect",
                        IsActive = true, IsPublic = false, DateOfCreate = DateTime.Now.AddDays(-8), UseCount = 16, ProjectId = 3},
                    new Link() {Id = 9, OldLink = "https://weather.com/ru-BY/weather/today/l/Gomel+Gomel+Region?canonicalCityId=0cb313c8f451769307932e50d1a85f4231aabe968c4729887713abffe8148b1f",
                        Description = "Погода Гомель",
                        IsActive = true, IsPublic = true, DateOfCreate = DateTime.Now.AddDays(-7), UseCount = 22, ProjectId = 4},
                    new Link() {Id = 10, OldLink = "https://www.tnonline.com/20200609/139471/",
                         Description = "Times hs graduation",
                        IsActive = true, IsPublic = true, DateOfCreate = DateTime.Now.AddDays(-5), UseCount = 31, ProjectId = 5},
                    new Link() {Id = 11, OldLink = "https://www.lamoda.by/c/4153/default-women/?labels=39156&sf=227&zbs_content=w_center_1_817779_by_0619_w_main_beach",
                        Description = "Товары женщинам в подборке",
                        IsActive = true, IsPublic = false, DateOfCreate = DateTime.Now.AddDays(-11), UseCount = 11, ProjectId = 6},
                    new Link() {Id = 12, OldLink = "https://medcenter2.by/component/content/article?id=69",
                        Description = "Семья и здоровье",
                        IsActive = true, IsPublic = false, DateOfCreate = DateTime.Now, UseCount = 2, ProjectId = 7},
                    new Link() {Id = 13, OldLink = "https://www.lamoda.by/c/1262/default-premium-women/?zbs_content=w_tiz_more_1_802531_all_1301_more_w_premium",
                        Description = "Премиум товары",
                        IsActive = false, IsPublic = true, DateOfCreate = DateTime.Now.AddDays(-5), UseCount = 4, ProjectId = 6},
                    new Link() {Id = 14, OldLink = "https://edition.cnn.com/2020/06/25/business/wirecard-mastercard-visa-scandal/index.html",
                        Description = "bbc visa and mc",
                        IsActive = false, IsPublic = true, DateOfCreate = DateTime.Now.AddDays(-14), UseCount = 11, ProjectId = 5},
                    new Link() {Id = 15, OldLink = "https://www.bbc.com/russian/features-53141228",
                        Description = "bbc одесский убийца",
                        IsActive = true, IsPublic = false, DateOfCreate = DateTime.Now.AddDays(-39), UseCount = 201, ProjectId = 5}
               };
        }
    }
}
