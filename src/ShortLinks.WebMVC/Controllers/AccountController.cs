﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;


namespace ShortLinks.WebMVC.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService, IUserService userService)
        {
            _userService = userService;
            _accountService = accountService;
        }

        // GET: /<controller>/
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                //check user into db
                var user = await _accountService.FindUserAsync(model);
                if (user != null)
                {
                    //authenticate and redirect
                    await Authenticate($"{user.LastName} {user.FirstName}", user.Email);
                    return RedirectToAction("ShowMine", "Link");
                }
                else
                {
                    //add error
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                }
            }

            return View();
        }


        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                //create new user and authenticate
                await _userService.AddUserIntoDBAsync(model);
                await Authenticate($"{model.LastName} {model.FirstName}", model.Email);

                return RedirectToAction("ShowMine", "Link");
            }
            return View();
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> CheckEmail(string email)
        {
            var isExists = await _accountService.IsEmailExistAsync(email);
            return Json(!isExists);
        }

        private async Task Authenticate(string userName, string email)
        {
            //create claims
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName),
                new Claim(ClaimTypes.Email, email)
            };

            // sign in
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}