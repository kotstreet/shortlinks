﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ShortLinks.BLL.Services.Interfaces;
using System.Threading.Tasks;

namespace ShortLinks.WebMVC.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {
        private readonly IStatisticsService _statisticsService;
        private readonly IPersonalDataProtectionService _personalDataProtectionService;

        public StatisticsController(
            IStatisticsService statisticsService,
            IPersonalDataProtectionService personalDataProtectionService)
        {
            _statisticsService = statisticsService;
            _personalDataProtectionService = personalDataProtectionService;
        }

        [HttpGet]
        public async Task<IActionResult> Show()
        {
            var projects = await _statisticsService.GetTopProjectsAsync(User);
            ViewBag.Projects = new SelectList(projects, "Id", "Caption");

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> AllLinks()
        {
            var linkStatistics = await _statisticsService.GetOrderedUserLinksAsync(User);
            return View(linkStatistics);
        }

        [HttpGet]
        public async Task<IActionResult> LinksByProject(int projectId)
        {
            if (_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                var linkStatistics = await _statisticsService.GetOrderedLinksByProjectAsync(projectId);
                return View(linkStatistics);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        public async Task<IActionResult> AllProjects()
        {
            var projectStatistics = await _statisticsService.GetOrderedProjects(User);
            return View(projectStatistics);
        }

        [HttpGet]
        public async Task<IActionResult> ShowDiagram()
        {
            var links = await _statisticsService.GetLinksAsync(User);
            return View(links);
        }
    }
}
