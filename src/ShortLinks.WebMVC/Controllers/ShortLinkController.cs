﻿using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Services.Interfaces;
using System.Threading.Tasks;

namespace ShortLinks.WebMVC.Controllers
{
    public class ShortLinkController : Controller
    {
        private readonly ILinkService _linkService;

        public ShortLinkController(ILinkService linkService)
        {
            _linkService = linkService;
        }

        [HttpGet]
        [Route("l")]
        public async Task<IActionResult> RedirectToOldLink(int id)
        {
            if (!await _linkService.IsLinkExistAsync(id))
            {
                return NotFound();
            }

            var link = await _linkService.GetLinkAsync(id);
            if (link.IsActive)
            {
                await _linkService.IncreaseInQuantityAsync(id);
                return Redirect(link.OldLink);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
