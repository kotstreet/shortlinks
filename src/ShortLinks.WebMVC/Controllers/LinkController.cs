﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using System.Threading.Tasks;

namespace ShortLinks.WebMVC.Controllers
{
    [Authorize]
    public class LinkController : Controller
    {
        private readonly ILinkService _linkService;
        private readonly IProjectService _projectService;
        private readonly IPersonalDataProtectionService _personalDataProtectionService;

        public LinkController(
            IProjectService projectService,
            ILinkService linkService,
            IPersonalDataProtectionService personalDataProtectionService)
        {
            _projectService = projectService;
            _linkService = linkService;
            _personalDataProtectionService = personalDataProtectionService;
        }

        public async Task<IActionResult> AllLinks()
        {
            return View(await _linkService.GetAllLinksAsync());
        }

        public IActionResult UserLinks()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Add(int projectId)
        {
            if (_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                var project = await _projectService.GetProjectAsync(projectId);
                ViewBag.Project = project;
                return View();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Add(LinkModel model)
        {
            var projectId = await _linkService.AddLinkAsync(model);
            return RedirectToAction("Links", "Project", new { projectId });
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmDelete(int linkId)
        {
            if (_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                var link = await _linkService.GetLinkAsync(linkId);
                return PartialView(link);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int linkId)
        {
            //get link and id for link project
            var link = await _linkService.GetLinkAsync(linkId);
            var projectId = link.ProjectId;

            //delete link
            await _linkService.DeleteLinkAsync(link);

            return RedirectToAction("Links", "Project", new { projectId });
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int linkId)
        {
            if (_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                var link = await _linkService.GetLinkAsync(linkId);
                ViewBag.Link = link;
                return View();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(LinkModel link)
        {
            await _linkService.SaveLinkAsync(link);
            return RedirectToAction("Links", "Project", new { projectId = link.ProjectId });
        }

        [HttpGet]
        public async Task<IActionResult> Move(int linkId)
        {
            if (_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                //create list of projects
                var list = _projectService.GetProjects(User);
                ViewBag.Projects = new SelectList(list, "Id", "Caption");

                var link = await _linkService.GetLinkAsync(linkId);
                return PartialView(link);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Move(int linkId, int projectId)
        {
            await _linkService.MoveLinkAsync(linkId, projectId);

            return RedirectToAction("Links", "Project", new { projectId });
        }

        [HttpGet]
        public async Task<IActionResult> ShowMine()
        {
            var links = await _linkService.FindMyLinksAsync(User);

            return View(await _linkService.GetLinksAndDefaulProjectModelAsync(links, User));
        }

    }
}