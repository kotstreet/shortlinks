﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using System.Threading.Tasks;

namespace ShortLinks.WebMVC.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;
        private readonly IPersonalDataProtectionService _personalDataProtectionService;

        public ProjectController(
            IProjectService projectService,
            IPersonalDataProtectionService personalDataProtectionService
            )
        {
            _projectService = projectService;
            _personalDataProtectionService = personalDataProtectionService;
        }

        public IActionResult Show()
        {
            var projects = _projectService.GetProjects(User);

            return View(projects);
        }

        public async Task<IActionResult> Links(int projectId)
        {
            if (_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                var project = await _projectService.GetLinksByProjectIdAsync(projectId);

                return View(project);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Project project)
        {
            await _projectService.AddProjectAsync(project, User);

            return RedirectToAction("Show", "Project");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int projectId)
        {
            if (_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                var project = await _projectService.GetProjectAsync(projectId);
                return View(project);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Project project)
        {
            await _projectService.SaveProjectAsync(project);

            return RedirectToAction("Show", "Project");
        }


        [HttpGet]
        public async Task<IActionResult> ConfirmDelete(int projectId)
        {
            if (_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                var project = await _projectService.GetProjectAsync(projectId);
                return PartialView(project);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int projectId)
        {
            await _projectService.DeleteProjectAsync(projectId);

            return RedirectToAction("Show", "Project");
        }
    }
}
