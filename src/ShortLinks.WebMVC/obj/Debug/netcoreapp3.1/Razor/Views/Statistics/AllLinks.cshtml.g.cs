#pragma checksum "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "da3ad50cf3744a1373cef23ecd56781365941826"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Statistics_AllLinks), @"mvc.1.0.view", @"/Views/Statistics/AllLinks.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\_ViewImports.cshtml"
using ShortLinks.WebMVC.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\_ViewImports.cshtml"
using ShortLinks.BLL.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\_ViewImports.cshtml"
using ShortLinks.BLL.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"da3ad50cf3744a1373cef23ecd56781365941826", @"/Views/Statistics/AllLinks.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1f3d4e63b22f9382bcf990539856d306ee6119e1", @"/Views/_ViewImports.cshtml")]
    public class Views_Statistics_AllLinks : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<LinkStatisticsModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/javascript"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/MyScripts.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::ShortLinks.WebMVC.TagHelpers.NewLinkTagHelper __ShortLinks_WebMVC_TagHelpers_NewLinkTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
  

    ViewData["Title"] = "Статистика";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""col"">
    <br />
    <div class=""text-center m-sm-1"">
        <p class=""display-3"">Общая статистика по ссылкам</p>
        <p>В статистике представленны только ваши ссылки, которые существуют на данный момент</p>
    </div>
    <br />
    <div class="" card "">
        <div class=""card-header"" id=""headingOne"">
            <h5 class=""mb-0"">
                <a class=""btn-collapse "" data-toggle=""collapse"" href=""#generalInfo"" role=""button"" aria-expanded=""false"" aria-controls=""generalInfo"">
                    Показать общую статистику <i class=""fas fa-chevron-down""></i>
                </a>
            </h5>
        </div>
        <div class="" font-weight-bold collapse"" id=""generalInfo"">
            <div class="" card-body"">
                <p>Общее количество ссылок: <text class=""text-success"">");
#nullable restore
#line 24 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                                                  Write(Model.LinkCount);

#line default
#line hidden
#nullable disable
            WriteLiteral("</text> </p>\r\n                <p>Общее количество открытых ссылок: <text class=\"text-success\">");
#nullable restore
#line 25 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                                                           Write(Model.PublicLinkCount);

#line default
#line hidden
#nullable disable
            WriteLiteral("</text></p>\r\n                <p>Общее количество активных ссылок: <text class=\"text-success\">");
#nullable restore
#line 26 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                                                           Write(Model.ActiveLinkCount);

#line default
#line hidden
#nullable disable
            WriteLiteral("</text></p>\r\n                <p>Общее количество переходов: <text class=\"text-success\">");
#nullable restore
#line 27 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                                                     Write(Model.TotalUseAmount);

#line default
#line hidden
#nullable disable
            WriteLiteral("</text></p>\r\n                <p>Время создания первой существующей ссылки: <text class=\"text-success\">");
#nullable restore
#line 28 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                                                                    Write(Model.FirstLinkDate.ToString("dd-MM-yyyy HH:mm:ss"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</text></p>\r\n                <p>Время создания последней существующей ссылки: <text class=\"text-success\">");
#nullable restore
#line 29 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                                                                       Write(Model.LastLinkDate.ToString("dd-MM-yyyy HH:mm:ss"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</text></p>
            </div>
        </div>
    </div>
    <br />
    <div>
        <table class=""table"">
            <tr class=""font-weight-bold text-center text-secondary"">
                <td style=""width:20vw"">Укороченая ссылка</td>
                <td style=""width:40vw"">Описание</td>
                <td style=""width:10vw"">Количество переходов</td>
                <td style=""width:10vw"">Дата создания</td>
                <td style=""width:20vw""></td>
            </tr>
");
#nullable restore
#line 43 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
             if (Model.Links != null)
            {
                

#line default
#line hidden
#nullable disable
#nullable restore
#line 45 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                 foreach (var item in Model.Links)
                {
                    string @urlString = Url.Action("RedirectToOldLink", "ShortLink", new { id = item.Id }, "https");

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr class=\"text-black-50\">\r\n                        <td>\r\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("new-link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "da3ad50cf3744a1373cef23ecd567813659418268766", async() => {
                WriteLiteral("\r\n                                ");
#nullable restore
#line 51 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                           Write(urlString);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                            ");
            }
            );
            __ShortLinks_WebMVC_TagHelpers_NewLinkTagHelper = CreateTagHelper<global::ShortLinks.WebMVC.TagHelpers.NewLinkTagHelper>();
            __tagHelperExecutionContext.Add(__ShortLinks_WebMVC_TagHelpers_NewLinkTagHelper);
#nullable restore
#line 50 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
__ShortLinks_WebMVC_TagHelpers_NewLinkTagHelper.LinkId = item.Id;

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("link-id", __ShortLinks_WebMVC_TagHelpers_NewLinkTagHelper.LinkId, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                        </td>\r\n                        <td>");
#nullable restore
#line 54 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                       Write(item.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"text-center\">");
#nullable restore
#line 55 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                           Write(item.UseCount);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"text-center\">");
#nullable restore
#line 56 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                           Write(item.DateOfCreate.ToString("dd-MM-yyyy HH:mm:ss"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"row\">\r\n                            <div>\r\n");
#nullable restore
#line 59 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                 if (item.IsPublic)
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <button class=\"btn btn-link text-black-50\">\r\n                                        <i title=\"Данная ссылка видна на главной странице\" class=\"far fa-eye\"></i>\r\n                                    </button>\r\n");
#nullable restore
#line 64 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                }
                                else
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <button class=\"btn btn-link text-black-50\">\r\n                                        <i title=\"Данная ссылка не видна на главной странице\" class=\"far fa-eye-slash\"></i>\r\n                                    </button>\r\n");
#nullable restore
#line 70 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                            </div>\r\n                            <div>\r\n");
#nullable restore
#line 73 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                 if (item.IsActive)
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <button class=\"btn btn-link text-black-50\">\r\n                                        <i title=\"Данная ссылка активна\" class=\"fas fa-toggle-on\"></i>\r\n                                    </button>\r\n");
#nullable restore
#line 78 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                }
                                else
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <button class=\"btn btn-link text-black-50\">\r\n                                        <i title=\"Данная ссылка не активна\" class=\"fas fa-toggle-off\"></i>\r\n                                    </button>\r\n");
#nullable restore
#line 84 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                            </div>\r\n                            <button class=\"btn btn-link text-black-50\">\r\n                                <i");
            BeginWriteAttribute("title", " title=\"", 4503, "\"", 4541, 3);
            WriteAttributeValue("", 4511, "Исходная", 4511, 8, true);
            WriteAttributeValue(" ", 4519, "ссылка:", 4520, 8, true);
#nullable restore
#line 87 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
WriteAttributeValue(" ", 4527, item.OldLink, 4528, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"far fa-hand-point-down\" ></i>\r\n                            </button>\r\n                            <button class=\"btn btn-link text-black-50\">\r\n                                <i");
            BeginWriteAttribute("title", " title=\"", 4727, "\"", 4764, 2);
            WriteAttributeValue("", 4735, "Проект:", 4735, 7, true);
#nullable restore
#line 90 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
WriteAttributeValue(" ", 4742, item.Project.Caption, 4743, 21, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"far fa-folder\"></i>\r\n                            </button>\r\n                        </td>\r\n                    </tr>\r\n");
#nullable restore
#line 94 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                }

#line default
#line hidden
#nullable disable
#nullable restore
#line 94 "D:\C#\_IBA\ShortLinks\src\ShortLinks.WebMVC\Views\Statistics\AllLinks.cshtml"
                 
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </table>\r\n    </div>\r\n</div>\r\n\r\n\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "da3ad50cf3744a1373cef23ecd5678136594182616166", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
            }
            );
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<LinkStatisticsModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
