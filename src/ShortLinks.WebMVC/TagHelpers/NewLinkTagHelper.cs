﻿using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLinks.WebMVC.TagHelpers
{
    public class NewLinkTagHelper : TagHelper
    {
        public int LinkId { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            //set tag name
            output.TagName = "a";

            //set attributes for action
            output.Attributes.Add("href", $"/l?id={LinkId}");
            
            //set style attributes
            output.Attributes.Add("class", "text-black-50");
            output.Attributes.Add("style", "text-decoration: none");

            //set content
            //output.Content.SetContent($"Ссылка {LinkId}");
        }
    }
}
