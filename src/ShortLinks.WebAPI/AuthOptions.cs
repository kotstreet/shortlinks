﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace ShortLinks.WebAPI
{
    public static class AuthOptions
    {
        public const string ISSUER = "ShortLinkServer";
        public const string AUDIENCE = "SomeClient";
        public const string KEY = "very-super-mega cool key!";   
        public const int LIFETIME = 5; 

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY));
        }
    }
}