﻿namespace ShortLinks.WebAPI.Constants
{
    public static class ResponseMessages
    {
        // Common 
        public const string ModelIsNull = "Model was empty.";

        // Account controller 
        public const string UserIsNotExist = "Incorrect emaol or password(or both)";
        public const string EmailExist = "Email already existe.";
        public const string RegisterError = "Something is going wrong while the user registers.";

        // Link controller
        public const string ProjectHasNotLink = "The link does not exist for the project.";

        // Short link controller
        public const string LinkIsNotActive = "The link is not active just now.";
        public const string LinkIsNotExist = "The link does not exists.";
    }
}
