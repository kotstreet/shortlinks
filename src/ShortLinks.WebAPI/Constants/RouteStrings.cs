﻿namespace ShortLinks.WebAPI.Constants
{
    /// <summary>
    /// These are strings for route actions and controllers.
    /// </summary>
    public static class RouteStrings
    {
        // Common
        public const string ControllerApiRoute = "api/[controller]";

        public const string ControllerShortLinkRoute = "api/";

        // Account controller
        public const string LoginRoute = "login";

        public const string RegisterRoute = "register";

        // Link controller
        public const string AllLinksRoute = "allLinks";

        public const string ConfirmDeleteLinkRoute = "confirm";

        public const string DeleteLinkRoute = "delete";

        public const string EditLinkRoute = "edit";

        public const string AddLinkRoute = "add";

        public const string MoveLinkRoute = "move";

        public const string ShowMineLinkRoute = "showMine";

        // Project controller
        public const string LinksForProjectRoute = "links";

        public const string MineProjectsRoute = "show";

        public const string ConfirmDeleteProjectRoute = "confirm";

        public const string DeleteProjectRoute = "delete";

        public const string EditProjectRoute = "edit";

        public const string CreateProjectRoute = "add";


        // Short link controller
        public const string ShortLinkRoute = "l";

        // Statistic controller
        public const string ShowStatisticRoute = "show";

        public const string AllLinksStatisticRoute = "allLinks";

        public const string LinksByProjectStatisticRoute = "linksByProject";

        public const string AllProjectStatisticRoute = "allProjects";

        public const string DiagramStatisticRoute = "diagram";
    }
}
