﻿namespace ShortLinks.WebAPI.Constants
{
    /// <summary>
    /// These are the strings for setting swagger up.
    /// </summary>
    public static class SwaggerStrings
    {
        public const string SwaggerPrefix = "api";

        public const string SwaggerUrl = "/swagger/v1/swagger.json";

        public const string SwaggerName = "ShortLinks.WebAPI";

        public const string Version = "v1";

        public const string AuthorizationName = "Authorization";

        public const string APITitle = "Short link API";
    }
}
