﻿namespace ShortLinks.WebAPI.Constants
{
    /// <summary>
    /// Constants for jwt claim name. 
    /// </summary>
    public static class JwtClaimName
    {
        public const string UserName = "name";

        public const string UserId = "id";
    }
}
