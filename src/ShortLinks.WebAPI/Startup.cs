using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using ShortLinks.WebAPI.Constants;
using ShortLinks.BLL.EF;
using ShortLinks.BLL.Services;
using ShortLinks.BLL.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ShortLinks.WebAPI
{
    public class Startup
    {
        private const string DefaultConnection = "DefaultConnection";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Register database context.
            var sqlConnectionString = Configuration.GetConnectionString(DefaultConnection);
            services.AddDbContext<ApplicationContext>(options =>
                options.UseNpgsql(sqlConnectionString));

            // add my services
            services.AddTransient<IPersonalDataProtectionService, PersonalDataProtectionService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ILinkService, LinkService>();
            services.AddTransient<IStatisticsService, StatisticsService>();
            services.AddTransient<IAccountService, AccountService>();

            // add jwt bearer
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        // Issuer settings
                        ValidateIssuer = true,
                        ValidIssuer = AuthOptions.ISSUER,

                        // Audience settings
                        ValidateAudience = true,
                        ValidAudience = AuthOptions.AUDIENCE,

                        ValidateLifetime = true,

                        // Key settings
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true,
                    };
                });

            // add swagger
            services.AddSwaggerGen(options =>
            {
                // add swagger documents
                options.SwaggerDoc(SwaggerStrings.Version, new OpenApiInfo
                {
                    Title = SwaggerStrings.APITitle,
                    Version = SwaggerStrings.Version,
                });

                // It's for authorize possibility in swagger.
                options.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme
                {
                    Description = @$"JWT Authorization header using the Bearer scheme. 
                      {Environment.NewLine}Enter '{JwtBearerDefaults.AuthenticationScheme}' [space] and then your token in the text input below.
                      {Environment.NewLine}Example: '{JwtBearerDefaults.AuthenticationScheme} 12345abcdef'",
                    Name = SwaggerStrings.AuthorizationName,
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = JwtBearerDefaults.AuthenticationScheme,
                });

                // It's for add jwt in header(in swagger).
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                         new OpenApiSecurityScheme
                         {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = JwtBearerDefaults.AuthenticationScheme,
                            },
                            Scheme = JwtBearerDefaults.AuthenticationScheme,
                            Name = JwtBearerDefaults.AuthenticationScheme,
                            In = ParameterLocation.Header,
                         },
                         new List<string>()
                    }
                });

                // add xml comments to swagger.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(SwaggerStrings.SwaggerUrl, SwaggerStrings.SwaggerName);
                c.RoutePrefix = SwaggerStrings.SwaggerPrefix;
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
