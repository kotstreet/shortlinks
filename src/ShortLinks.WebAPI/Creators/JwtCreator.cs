﻿using Microsoft.IdentityModel.Tokens;
using ShortLinks.WebAPI.Constants;
using ShortLinks.BLL.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace ShortLinks.WebAPI.Creators
{
    /// <summary>
    /// Creator of JSON web token.
    /// </summary>
    public class JwtCreator
    {
        /// <summary>
        /// Generate new jwt bearer token for the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Generated token.</returns>
        public static string GenerateJwtToken(User user)
        {
            // setting claims
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtClaimName.UserName, $"{user.FirstName} {user.LastName}"),
                new Claim(JwtClaimName.UserId, user.Id.ToString()),
            };

            // setting jwt token
            var token = new JwtSecurityToken(
                AuthOptions.ISSUER,
                AuthOptions.AUDIENCE,
                claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddDays(AuthOptions.LIFETIME),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
