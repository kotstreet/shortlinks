﻿using ShortLinks.BLL.Models;
using ShortLinks.BLL.ViewModels;

namespace ShortLinks.WebAPI.Creators
{
    /// <summary>
    /// Creator of user.
    /// </summary>
    public static class UserCreator
    {

        /// <summary>
        /// Create new user by register madel.
        /// </summary>
        /// <param name="model">Register model.</param>
        /// <returns>Created user.</returns>
        public static User CreateNewUser(RegisterModel model) =>
            new User
            {
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Password = model.Password,
            };
    }
}
