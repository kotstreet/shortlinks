﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.WebAPI.Constants;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.Controllers
{
    /// <summary>
    /// Controller for display statistic.
    /// </summary>
    /// <response code="401">User not authenticate.</response>
    [Authorize()]
    [Route(RouteStrings.ControllerApiRoute)]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        private readonly IStatisticsService _statisticsService;
        private readonly IPersonalDataProtectionService _personalDataProtectionService;

        public StatisticsController(
            IStatisticsService statisticsService,
            IPersonalDataProtectionService personalDataProtectionService)
        {
            _statisticsService = statisticsService;
            _personalDataProtectionService = personalDataProtectionService;
        }

        /// <summary>
        /// Get top projects for the current user.
        /// </summary>
        /// <returns>Ordered projects.</returns>
        /// <response code="200">All is OK.</response>
        [HttpGet]
        [Route(RouteStrings.ShowStatisticRoute)]
        public async Task<IActionResult> Show()
        {
            return Ok(await _statisticsService.GetTopProjectsAsync(User));
        }

        /// <summary>
        /// Get all links for the current user.
        /// </summary>
        /// <returns>All links.</returns>
        /// <response code="200">All is OK.</response>
        [HttpGet]
        [Route(RouteStrings.AllLinksStatisticRoute)]
        public async Task<IActionResult> AllLinks()
        {
            return Ok(await _statisticsService.GetOrderedUserLinksAsync(User));
        }

        /// <summary>
        /// Get info aboute links by the project.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>Ordered links.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.LinksByProjectStatisticRoute)]
        public async Task<IActionResult> LinksByProject(int projectId)
        {
            if (!_personalDataProtectionService.IsProjectExists(projectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _statisticsService.GetOrderedLinksByProjectAsync(projectId));
            }
        }

        /// <summary>
        /// Get all projcts for the current user.
        /// </summary>
        /// <returns>Ordered projects.</returns>
        /// <response code="200">All is OK.</response>
        [HttpGet]
        [Route(RouteStrings.AllProjectStatisticRoute)]
        public async Task<IActionResult> AllProjects()
        {
            return Ok(await _statisticsService.GetOrderedProjects(User));
        }

        /// <summary>
        /// Get links for display diagram statistic.
        /// </summary>
        /// <returns>All user links.</returns>
        /// <response code="200">All is OK.</response>
        [HttpGet]
        [Route(RouteStrings.DiagramStatisticRoute)]
        public async Task<IActionResult> ShowDiagram()
        {
            return Ok(await _statisticsService.GetLinksAsync(User));
        }
    }
}
