﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using ShortLinks.WebAPI.Constants;
using ShortLinks.WebAPI.Creators;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.Controllers
{
    /// <summary>
    /// Controlle for manipulation authentication.
    /// </summary>
    [AllowAnonymous]
    [Route(RouteStrings.ControllerApiRoute)]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        /// <summary>
        /// Authenticate user on the site and return jwt for him.
        /// </summary>
        /// <param name="model">Model for user login.</param>
        /// <returns>JWT for the user.</returns>
        /// <response code="400">The entered data were bad.</response>
        /// <response code="200">All is OK.</response>
        [Route(RouteStrings.LoginRoute)]
        [HttpPost]
        public async Task<object> Login([FromBody] LoginModel model)
        {
            // check for valid model state
            if (!ModelState.IsValid || model == null)
            {
                return BadRequest(ModelState);
            }

            // check for user existance
            var user = await _accountService.FindUserAsync(model);
            if (user == null)
            {
                return BadRequest(ResponseMessages.UserIsNotExist);
            }
            else
            {
                return JwtCreator.GenerateJwtToken(user);
            }
        }

        /// <summary>
        /// Create a new user in the database and return token for the user.
        /// </summary>
        /// <param name="model">Model for registrate a new user.</param>
        /// <returns>JWT for the user.</returns>
        /// <response code="400">The entered data were bad.</response>
        /// <response code="200">All is OK.</response>
        [Route(RouteStrings.RegisterRoute)]
        [HttpPost]
        public async Task<object> Register([FromBody] RegisterModel model)
        {
            // check for valid model state
            if (!ModelState.IsValid || model == null)
            {
                return BadRequest(ModelState);
            }

            // check for unique email
            if (await _accountService.IsEmailExistAsync(model.Email))
            {
                return BadRequest(ResponseMessages.EmailExist);
            }
            else
            {
                // add user
                var user = UserCreator.CreateNewUser(model);
                await _accountService.CreateUserAsync(user);

                return JwtCreator.GenerateJwtToken(await _accountService.FindUserAsync(user.Email));
            }
        }
    }
}
