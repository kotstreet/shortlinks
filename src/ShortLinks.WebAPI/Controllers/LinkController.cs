﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.BLL.ViewModels;
using ShortLinks.WebAPI.Constants;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.Controllers
{
    /// <summary>
    /// Controller for manipulations with links.
    /// </summary>
    /// <response code="401">User not authenticate.</response>
    [Authorize()]
    [Route(RouteStrings.ControllerApiRoute)]
    [ApiController]
    public class LinkController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly ILinkService _linkService;
        private readonly IPersonalDataProtectionService _personalDataProtectionService;

        public LinkController(
            ILinkService linkService,
            IProjectService projectService,
            IPersonalDataProtectionService personalDataProtectionService)
        {
            _linkService = linkService;
            _projectService = projectService;
            _personalDataProtectionService = personalDataProtectionService;
        }

        /// <summary>
        /// Get all links from database.
        /// </summary>
        /// <returns>List of links.</returns>
        /// <response code="200">All is OK.</response>
        [Route(RouteStrings.AllLinksRoute)]
        [HttpGet]
        public async Task<IActionResult> AllLinks()
        {
            return Ok(await _linkService.GetAllLinksAsync());
        }


        /// <summary>
        /// Get the project info.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.AddLinkRoute)]
        public async Task<IActionResult> Add(int projectId)
        {
            if (!_personalDataProtectionService.IsProjectExists(projectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _projectService.GetProjectAsync(projectId));
            }
        }

        /// <summary>
        /// Add a new link to the project from LinkModel.
        /// </summary>
        /// <param name="model">LinkModel that contains info aboute new link and the project.</param>
        /// <returns>Id of the link project.</returns>
        /// <response code="200">item was created successfully.</response>
        /// <response code="400">The entered data were bad.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpPost]
        [Route(RouteStrings.AddLinkRoute)]
        public async Task<IActionResult> Add([FromBody] LinkModel model)
        {
            if (model == null)
            {
                return BadRequest(ResponseMessages.ModelIsNull);
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_personalDataProtectionService.IsProjectExists(model.ProjectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(model.ProjectId, User))
            {
                return Forbid();
            }
            else
            {
                var projectId = await _linkService.AddLinkAsync(model);
                return Ok(projectId);
            }
        }

        /// <summary>
        /// Get info aboute the link for delete.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <returns>The Link.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The link does not exist for this user.</response>
        /// <response code="404">The link does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.ConfirmDeleteLinkRoute)]
        public async Task<IActionResult> ConfirmDeleteLink(int linkId)
        {
            if (!await _linkService.IsLinkExistAsync(linkId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _linkService.GetLinkAsync(linkId));
            }
        }

        /// <summary>
        /// Delete the link from database.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <returns>Project id for deleted link.</returns>
        /// <response code="200">Item was deleted succesfully.</response>
        /// <response code="403">The link does not exist for this user.</response>
        /// <response code="404">The link does not exist at all.</response>
        [HttpDelete]
        [Route(RouteStrings.DeleteLinkRoute)]
        public async Task<IActionResult> Delete(int linkId)
        {
            if (!await _linkService.IsLinkExistAsync(linkId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                return Forbid();
            }
            else
            {
                // get link and project id
                var link = await _linkService.GetLinkAsync(linkId);
                var projectId = link.ProjectId;

                //delete link
                await _linkService.DeleteLinkAsync(link);
                return Ok(projectId);
            }
        }

        /// <summary>
        /// Get info aboute the link for edit.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <returns>The Link.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The link does not exist for this user.</response>
        /// <response code="404">The link does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.EditLinkRoute)]
        public async Task<IActionResult> Edit(int linkId)
        {
            if (!await _linkService.IsLinkExistAsync(linkId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _linkService.GetLinkAsync(linkId));
            }
        }

        /// <summary>
        /// Edit info aboute the link.
        /// </summary>
        /// <param name="link">Link model for edit.</param>
        /// <returns>Project id for the link.</returns>
        /// <response code="200">Item was update successfully.</response>
        /// <response code="400">Model is bad.</response>
        /// <response code="403">The link does not exist for this user.</response>
        /// <response code="404">The link does not exist at all.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpPut]
        [Route(RouteStrings.EditLinkRoute)]
        public async Task<IActionResult> Edit(LinkModel link)
        {
            if (link == null)
            {
                return BadRequest(ResponseMessages.ModelIsNull);
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_personalDataProtectionService.IsProjectExists(link.ProjectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(link.ProjectId, User))
            {
                return Forbid();
            }

            if (!await _linkService.IsLinkExistAsync(link.Id))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasLink(link.Id, User))
            {
                return Forbid();
            }
            else
            {
                await _linkService.SaveLinkAsync(link);
                return Ok(link.ProjectId);
            }
        }

        /// <summary>
        /// Get info for move the link to another project.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <returns>Info for move the link</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The link does not exist for this user.</response>
        /// <response code="404">The link does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.MoveLinkRoute)]
        public async Task<IActionResult> Move(int linkId)
        {
            if (!await _linkService.IsLinkExistAsync(linkId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _linkService.CreateMoveLinkModelAsync(linkId));
            }
        }

        /// <summary>
        /// Move the link to the project.
        /// </summary>
        /// <param name="linkId">The link id.</param>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project id.</returns>
        /// <response code="200">Item was moved successfully.</response>
        /// <response code="400">Link does not exist for the project.</response>
        /// <response code="403">The link does not exist for this user.</response>
        /// <response code="404">The link does not exist at all.</response>
        [HttpPut]
        [Route(RouteStrings.MoveLinkRoute)]
        public async Task<IActionResult> Move(int linkId, int projectId)
        {
            if (!await _linkService.IsLinkExistAsync(linkId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasLink(linkId, User))
            {
                return Forbid();
            }
            else if (!await _linkService.IsExistLinkInProjectAsync(linkId, projectId))
            {
                return BadRequest(ResponseMessages.ProjectHasNotLink);
            }
            else
            {
                await _linkService.MoveLinkAsync(linkId, projectId);
                return Ok(projectId);
            }
        }

        /// <summary>
        /// Return all user links.
        /// </summary>
        /// <returns>List of links.</returns>
        /// <response code="200">All is OK.</response>
        [HttpGet]
        [Route(RouteStrings.ShowMineLinkRoute)]
        public async Task<IActionResult> ShowMine()
        {
            return Ok(await _linkService.FindMyLinksAsync(User));
        }
    }
}
