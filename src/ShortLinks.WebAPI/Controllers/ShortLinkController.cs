﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.WebAPI.Constants;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.Controllers
{
    /// <summary>
    /// Controller for controle redirect to link.
    /// </summary>
    [AllowAnonymous]
    [Route(RouteStrings.ControllerShortLinkRoute)]
    [ApiController]
    public class ShortLinkController : ControllerBase
    {
        private readonly ILinkService _linkService;

        public ShortLinkController(ILinkService linkService)
        {
            _linkService = linkService;
        }

        /// <summary>
        /// Redirect to the old link.
        /// </summary>
        /// <param name="id">The link id.</param>
        /// <returns>Old url for the link.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="404">Link is not active just now or does not exist.</response>
        [HttpGet]
        [Route(RouteStrings.ShortLinkRoute)]
        public async Task<IActionResult> RedirectToOldLink(int id)
        {
            if (!await _linkService.IsLinkExistAsync(id))
            {
                return NotFound(ResponseMessages.LinkIsNotExist);
            }

            var link = await _linkService.GetLinkAsync(id);
            if (link.IsActive)
            {
                await _linkService.IncreaseInQuantityAsync(id);
                return Ok(link.OldLink);
            }
            else
            {
                return NotFound(ResponseMessages.LinkIsNotActive);
            }
        }
    }
}
