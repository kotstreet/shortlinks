﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShortLinks.BLL.Models;
using ShortLinks.BLL.Services.Interfaces;
using ShortLinks.WebAPI.Constants;
using System.Threading.Tasks;

namespace ShortLinks.WebAPI.Controllers
{
    /// <summary>
    /// Controller for manipulate projects.
    /// </summary>
    /// <response code="401">User not authenticate.</response>
    [Authorize()]
    [Route(RouteStrings.ControllerApiRoute)]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly IPersonalDataProtectionService _personalDataProtectionService;

        public ProjectController(
            IProjectService projectService,
            IPersonalDataProtectionService personalDataProtectionService
            )
        {
            _projectService = projectService;
            _personalDataProtectionService = personalDataProtectionService;
        }

        /// <summary>
        /// Get all projects for the current user.
        /// </summary>
        /// <returns>Projects for the current user.</returns>
        /// <response code="200">All is OK.</response>
        [HttpGet]
        [Route(RouteStrings.MineProjectsRoute)]
        public IActionResult Show()
        {
            return Ok(_projectService.GetProjects(User));
        }

        /// <summary>
        /// Get all links for the project.
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <returns>All links.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.LinksForProjectRoute)]
        public async Task<IActionResult> Links(int projectId)
        {
            if (!_personalDataProtectionService.IsProjectExists(projectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _projectService.GetLinksByProjectIdAsync(projectId));
            }
        }

        /// <summary>
        /// Create a new project. 
        /// </summary>
        /// <param name="project">The project for create.</param>
        /// <returns>Status code.</returns>
        /// <response code="201">Item was created successfully.</response>
        /// <response code="400">Model is not valid.</response>
        [HttpPost]
        [Route(RouteStrings.CreateProjectRoute)]
        public async Task<IActionResult> Create(Project project)
        {
            if (project == null)
            {
                return BadRequest(ResponseMessages.ModelIsNull);
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                await _projectService.AddProjectAsync(project, User);
                return Created(nameof(Show), null);
            }
        }

        /// <summary>
        /// Get the project for edit.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.EditProjectRoute)]
        public async Task<IActionResult> Edit(int projectId)
        {
            if (!_personalDataProtectionService.IsProjectExists(projectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _projectService.GetProjectAsync(projectId));
            }
        }

        /// <summary>
        /// Edit info aboute the project.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns>Status code.</returns>
        /// <response code="200">Item was updated successfully.</response>
        /// <response code="400">Model is not valid.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpPut]
        [Route(RouteStrings.EditProjectRoute)]
        public async Task<IActionResult> Edit(Project project)
        {
            if (project == null)
            {
                return BadRequest(ResponseMessages.ModelIsNull);
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_personalDataProtectionService.IsProjectExists(project.Id))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(project.Id, User))
            {
                return Forbid();
            }
            else
            {
                await _projectService.SaveProjectAsync(project);
                return Ok();
            }
        }

        /// <summary>
        /// Get the project for edit.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project.</returns>
        /// <response code="200">All is OK.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpGet]
        [Route(RouteStrings.ConfirmDeleteProjectRoute)]
        public async Task<IActionResult> ConfirmDelete(int projectId)
        {
            if (!_personalDataProtectionService.IsProjectExists(projectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                return Forbid();
            }
            else
            {
                return Ok(await _projectService.GetProjectAsync(projectId));
            }
        }

        /// <summary>
        /// Delete project with all links.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>Status code.</returns>
        /// <response code="204">Item was deleted successfully.</response>
        /// <response code="403">The project does not exist for this user.</response>
        /// <response code="404">The project does not exist at all.</response>
        [HttpDelete]
        [Route(RouteStrings.DeleteProjectRoute)]
        public async Task<IActionResult> Delete(int projectId)
        {
            if (!_personalDataProtectionService.IsProjectExists(projectId))
            {
                return NotFound();
            }
            else if (!_personalDataProtectionService.IsUserHasProject(projectId, User))
            {
                return Forbid();
            }
            else
            {
                await _projectService.DeleteProjectAsync(projectId);
                return NoContent();
            }
        }
    }
}